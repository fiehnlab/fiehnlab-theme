'use strict';

angular.module('app', ['ngAnimate', 'ui.bootstrap'])

    .controller('NavigationController', ['$scope', function($scope) {
        $scope.navCollapsed = true;
    }]);